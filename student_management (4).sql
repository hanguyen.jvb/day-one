-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2020 at 01:26 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `dtb_class`
--

CREATE TABLE `dtb_class` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dtb_class`
--

INSERT INTO `dtb_class` (`class_id`, `class_name`, `department_id`) VALUES
(1, 'Công nghệ 1', 1),
(2, 'Điện-Điện tử 1', 2),
(3, 'Kế toán 1', 3),
(4, 'Viễn thông 1', 4);

-- --------------------------------------------------------

--
-- Table structure for table `dtb_department`
--

CREATE TABLE `dtb_department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dtb_department`
--

INSERT INTO `dtb_department` (`department_id`, `department_name`) VALUES
(1, ' Công nghệ'),
(2, 'Điện tử'),
(3, 'Kế toán '),
(4, 'Viễn thông \r\n');

-- --------------------------------------------------------

--
-- Table structure for table `dtb_students`
--

CREATE TABLE `dtb_students` (
  `student_id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dtb_students`
--

INSERT INTO `dtb_students` (`student_id`, `full_name`, `email`, `phone`, `birthday`, `gender`, `class_id`, `department_id`, `password`, `username`, `avatar`) VALUES
(58, 'Hoang Tien Anhh', 'tienanh@gmail.com', '0367567345', '1997-08-08', 1, 1, 1, '123456', 'anhht97', 'anhht.jpg'),
(59, 'Nguyen Thu Thao', 'thao@gmail.com', '0945345678', '1997-05-19', 0, 3, 3, '123456', 'thaont97', 'thaont-2.jpg'),
(60, 'kien vu trung', 'kien@gmail.com', '0167557456', '1997-11-05', 1, 2, 2, '123456', 'kienvt97', 'kienvt.jpg'),
(61, 'linh ngo phuong', 'linh@gmail.com', '0167557475', '1997-10-04', 0, 3, 3, '123456', 'linhnp97', 'linhnp.jpg'),
(62, 'Nguyen Phuong Thuy', 'thuy@gmail.com', '0433567976', '1997-07-27', 0, 2, 2, '123456', 'thuynp97', 'thuynp.jpg'),
(63, 'trang huyen cao', 'trang@gmail.com', '0367567345', '1997-07-24', 0, 3, 3, '123456', 'tranghuyen97', 'trangch.jpg'),
(64, 'Nhung hoang hong', 'nhung@gmail.com', '0167456197', '0008-01-18', 0, 3, 3, '123456', 'nhunghh97', 'nhunghh.jpg'),
(65, 'vinh xuan nguyen', 'vinh@gmail.com', '0367876345', '1998-11-19', 1, 1, 1, '123456', 'vinhnx98', 'vinhnx.jpg'),
(66, 'huy pham quang', 'huy@gmail.com', '0169345123', '1997-06-16', 1, 4, 4, '123456', 'huypq97', 'huypq.jpg'),
(67, 'nguyen manh hiep', 'hiep@gmail.com', '0123456765', '2001-12-21', 1, 4, 4, '123456', 'hiep2k', 'hiepnm.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dtb_class`
--
ALTER TABLE `dtb_class`
  ADD PRIMARY KEY (`class_id`),
  ADD UNIQUE KEY `name` (`class_name`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `dtb_department`
--
ALTER TABLE `dtb_department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `dtb_students`
--
ALTER TABLE `dtb_students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `department_id` (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dtb_class`
--
ALTER TABLE `dtb_class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dtb_department`
--
ALTER TABLE `dtb_department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dtb_students`
--
ALTER TABLE `dtb_students`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dtb_class`
--
ALTER TABLE `dtb_class`
  ADD CONSTRAINT `department_id` FOREIGN KEY (`department_id`) REFERENCES `dtb_department` (`department_id`);

--
-- Constraints for table `dtb_students`
--
ALTER TABLE `dtb_students`
  ADD CONSTRAINT `dtb_students_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `dtb_class` (`class_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `dtb_students_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `dtb_department` (`department_id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
