<?php
    class LogoutController extends Controller
    {
        public function logout()
        {
            if(isset($_SESSION['name']) && isset($_SESSION['password'])) {
                session_destroy();
                header('location: ../LoginController/index');
            }
        }
    }
?>
