<?php
    class Database
    {
        public $conn;
        public $servername = "localhost";
        public $username = "root";
        public $password = "";
        public $db = "student_management";

        function __construct()
        {
            $this->conn = mysqli_connect($this->servername, $this->username, $this->password, $this->db);
            return mysqli_query($this->conn, "SET NAMES 'utf8'");
        }

        public function select($sql)
        {
            $result = mysqli_query($this->conn,$sql);
            $numRow = mysqli_num_rows($result);
            
            if($numRow > 0) {
                return true;
            }
            return false;
        }
    }
?>
