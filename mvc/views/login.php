<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="/day-one/php-basic/public/css/login.css">
</head>
<body>
    <div id="login">
        <h2 class="title-login">Login to JVB</h2>
        <div class="login-box">
            <form id="login-form" method="POST">
                <h3>Login</h3>
                <span class="error-login"><?php if(isset($data['errorLogin'])) echo ($data['errorLogin'])?></span>
                <input type="text" name="username" id="username" placeholder="username">
                <input type="password" name="password" id="password" placeholder="password">       
                <input type="submit" name="submit" class="btn-login" value="login">    
            </form>                            
        </div>
    </div>
</body>
</html>
