<div class="search-student">
    <form action="./StudentController/search" method="post">
        <table>
            <tr>
                <td><input type="text" class="key-search" name="key_search" placeholder="The name you wanna search"></td>
                <td><input type="submit" class="btn-search" name="search" value="search"></td>
            </tr>
        </table>
    </form>
</div>

<div class="add-student">
    <a href="StudentController/add" class="btn-add-student">Add Student +</a>
</div>
<form method="POST" action="StudentController/delete" onsubmit="return deleteConfirm();">
    <table id="table-student">
        <tr>
            <th>Id</th>
            <th>Avatar</th>
            <th>Fullname</th>
            <th>Username</th>
            <th>Email</th>
            <th>Class</th>
            <th>Department</th>
            <th>Phone</th>
            <th>Birthday</th>
            <th>Gender</th>
            <th>Edit</th>
            <th><input type="checkbox" name="select_all" id="select_all"></th>
        </tr>
        <tr>
            <?php
                while ($student = mysqli_fetch_array($data['listLimitStudent'])) { ?>
                    <tr>
                        <td><?php echo $student['student_id']; ?></td>
                        <td><img class="avatar" src="../image/<?php echo $student['avatar'] ?>"></td>
                        <td><?php echo $student['full_name']; ?></td>
                        <td><?php echo $student['username']; ?></td>
                        <td><?php echo $student['email']; ?></td>
                        <td><?php echo $student['class_name']; ?></td>
                        <td><?php echo $student['department_name']; ?></td>
                        <td><?php echo $student['phone']; ?></td>
                        <td><?php echo $student['birthday']; ?></td>
                        <td>
                            <?php 
                                if ($student['gender'] == 1) {
                                    echo "Nam";
                                } else {
                                    echo "Nữ";
                                }
                            ?>
                        </td>
                        <td><a href="StudentController/edit/<?php echo $student['student_id']; ?>"><i class="fas fa-pen" style="color: #008B8B"></i></a></td>
                        <td><input type="checkbox" name="checkbox_id[]" class="checkbox" value="<?php echo $student['student_id']; ?>"></td>
                    </tr>          
            <?php } ?>
            <input type="submit" name="delete-student" id="delete-student" value="Delete Student"> 
        <tr>
    </table>
</form>

<div class="pagination">
    <?php
        $countStudent = mysqli_num_rows($data['listStudent']);
        $totalPage = ceil($countStudent / 3);
        $page = 1;
        for ($page = 1; $page <= $totalPage; $page++) { 
    ?> 
        <a href="/day-one/php-basic/StudentController/index/<?php echo $page; ?>"><?php echo $page; ?></a>
        <?php } ?>
</div>
