<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Student</title>
</head>
<body>
    <div id="edit-student">
        <div class="edit-student-box">
            <?php $oneStudent = mysqli_fetch_array($data['listOneStudent']) ?>
            <form class="edit-student-form" method="POST" id="formEditStudent" enctype="multipart/form-data">
                <h3>Edit Student</h3>
                <input type="text" name="fullname" id="fullname" placeholder="fullname" value="<?php echo $oneStudent['full_name']; ?>">
                <input type="text" name="username" id="username" placeholder="username" value="<?php echo $oneStudent['username']; ?>">
                <input type="email" name="email" id="email" placeholder="email" value="<?php echo $oneStudent['email']; ?>">
                <input type="password" name="password" id="password" placeholder="password" value="<?php echo $oneStudent['password']; ?>">
                <select id="class-student" name="class_student">
                    <option value="1" <?php if($oneStudent['class_id'] == 1) echo 'selected'; ?>>Công nghệ 1 - class</option>
                    <option value="2" <?php if($oneStudent['class_id'] == 2) echo 'selected'; ?>>Điện-Điện tử 1 - class</option>
                    <option value="3" <?php if($oneStudent['class_id'] == 3) echo 'selected'; ?>>Kế toán 1 - class</option>
                    <option value="4" <?php if($oneStudent['class_id'] == 4) echo 'selected'; ?>>Viễn thông 1 - class</option>
                </select>
                <select id="department-student" name="department_student">
                    <option value="1" <?php if($oneStudent['department_id'] == 1) echo 'selected'; ?>>Công nghệ - department</option>
                    <option value="2" <?php if($oneStudent['department_id'] == 2) echo 'selected'; ?>>Điện-Điện tử - department</option>
                    <option value="3" <?php if($oneStudent['department_id'] == 3) echo 'selected'; ?>>Kế toán - department</option>
                    <option value="4" <?php if($oneStudent['department_id'] == 4) echo 'selected'; ?>>Viễn thông - department</option>
                </select>
                <label for="uploadAvatar" style="font-size: 19px;">Update Avatar(just update png, jpg, jpeg)</label>
                <input type="file" name="fileToUpdate" id="fileToUpdate">
                <div class="empty-text">Update images
                    <img src="../image/<?php echo $oneStudent['avatar']; ?>" alt="">
                </div>
                <input type="number" name="phone" id="phone" placeholder="your phone" value="<?php echo $oneStudent['phone']; ?>">
                <input type="date" name="birthday" id="birthday" placeholder="your birthday" value="<?php echo $oneStudent['birthday']; ?>">
                <div>
                    <input type="radio" id="male" name="gender" value="1" <?php if($oneStudent['gender'] == 1) echo 'checked'; ?>>
                    <label for="male">Male</label>
                    <input type="radio" id="female" name="gender" value="0" <?php if($oneStudent['gender'] == 0) echo 'checked'; ?>>
                    <label for="female">Female</label>
                </div>
                <input type="hidden" name="student_id" value="<?php echo $oneStudent["student_id"]; ?>">
                <input type="submit" name="edit-student" class="btn-edit">
            </form>                            
        </div>
    </div>
</body>
</html>

<script>
    $('#fileToUpdate').on('change', function() {
        var file = this.files[0];
        var imagefile = file.type;
        var imageTypes = ["image/jpeg", "image/png", "image/jpg"];
        if (imageTypes.indexOf(imagefile) == -1) {
            return false;
            $(this).empty();
        } else {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(".empty-text").html('<img src="' + e.target.result + '"  />');
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
</script>
