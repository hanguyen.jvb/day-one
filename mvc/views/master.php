<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <base href="/day-one/php-basic/">
    <link rel="stylesheet" href="public/css/sidebar.css">
    <link rel="stylesheet" href="public/css/header.css">
    <link rel="stylesheet" href="public/css/list-student.css">
    <link rel="stylesheet" href="public/css/add-student.css">
    <link rel="stylesheet" href="public/css/edit-student.css">
    <link rel="stylesheet" href="public/css/error-404.css">
    <link rel="stylesheet" href="public/css/all.min.css">
    <script src="public/js/all.min.js"></script> 
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/jquery.validate.min.js"></script>
    <script src="public/js/validate/addStudent.js"></script>
    <script src="public/js/validate/editStudent.js"></script>
    <script src="public/js/delete/student.js"></script>
    <script src="public/js/update/classByDepartment.js"></script>
</head>
<body>
    <?php require_once "./mvc/views/blocks/header.php"; ?>
    <?php require_once "./mvc/views/blocks/sidebar.php"; ?>

    <div id="content">
        <?php
            if (isset($data['pageStudent'])) {
                require_once "./mvc/views/pages/" . $data['pageStudent'] . ".php";
            }

            if (isset($data['pageAddUser'])) {
                require_once "./mvc/views/pages/" . $data['pageAddUser'] . ".php";
            }

            if (isset($data['pageEditUser'])) {
                require_once "./mvc/views/pages/" . $data['pageEditUser'] . ".php";
            }

            if (isset($data['pageSearchUser'])) {
                require_once "./mvc/views/pages/" . $data['pageSearchUser'] . ".php";
            }

            if (isset($data['pageErrorUser'])) {
                require_once "./mvc/views/pages/" . $data['pageErrorUser'] . ".php";
            }
        ?>
    </div>
</body>
</html>
